var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var twig = require('twig');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var venituriRouter = require("./routes/venituri");
var cheltuieliRouter = require("./routes/cheltuieli");
var conturiRouter = require("./routes/conturi");
var conturiModel = require("./models/conturi");
var economiiRouter = require('./routes/economii');
var mongoose = require('mongoose');
var expressValidator = require('express-validator');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var utilizatoriRouter = require('./routes/utilizatori')(passport);

var app = express();

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/encrypted", { useNewUrlParser: true });
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');
app.set('twig options', { 
  strict_variables: false
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// add validation methods to request
app.use(session({
  secret: 'MySuperSecretSessionText',
  resave: false,
  saveUninitialized: true
}));

app.use(flash());
require('./config/passport.js')(app, passport);
app.use(async function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user;
  if(req.user){
      res.locals.conturiUser = await conturiModel.find({ user: req.user });
  }
  next();
});

app.use(expressValidator({
  customValidators: {
      dropdownValidator : function (value, values){
          if(value)
              return (typeof values[value] !== 'undefined');
          else
              return true;
      }
  }
}));
 
app.use('/', indexRouter);
app.use('/users', utilizatoriRouter);
app.use('/conturi', conturiRouter); //conturi
app.use('/venituri', venituriRouter); // /venituri
app.use('/cheltuieli', cheltuieliRouter); // /cheltuieli
app.use('/economii', economiiRouter); // /economii

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
