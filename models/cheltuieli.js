let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
	
let cheltuieliSchema = new Schema({
    suma: { type: Number, required: true},
    date: { type: String, default: new Date()},
    categoria: {type: String, required: true},
    cont: { type: String },
    user: { type: mongoose.Schema.Types.ObjectId, ref:'User' },
    moneda: {type: String, required: true}
});
cheltuieliSchema.statics.validateForm = async function (req) {
    let monede = {"RON":"RON", "EUR":"EUR"}
    req.checkBody('moneda', 'Moneda pe care ai ales-o nu face parte din cele date').dropdownValidator(monede);
    req.checkBody('moneda', 'Moneda este obligatorie').notEmpty();
    req.checkBody('suma', 'Suma este obligatorie').notEmpty();
    req.checkBody('suma', 'Suma trebuie să fie un număr').isNumeric(monede);
    req.checkBody('cont', 'Contul este obligatoriu').notEmpty(monede);
    req.checkBody('categoria','Categoria este obligatorie').notEmpty();

    let errors = await req.getValidationResult();
    return errors.throw();
};

let Model = mongoose.model('Cheltuieli', cheltuieliSchema);
module.exports = Model;