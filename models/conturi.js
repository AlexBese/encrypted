let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
	
let conturiSchema = new Schema({
    numar: { type: String, required: true},
    user: { type: mongoose.Schema.Types.ObjectId, ref:'User' }
});

conturiSchema.statics.validateForm = async function (req) {
    req.checkBody('numar', 'Trebuie să introduceți un număr').notEmpty();

    let errors = await req.getValidationResult();
    return errors.throw();
};

let Model = mongoose.model('Cont', conturiSchema);
module.exports = Model;