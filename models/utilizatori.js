'use strict';

let mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

let userSchema = new Schema({
        username: {type: String},
        phone: {type: String},
        city: {type: String},
        email: {type: String},
        password: {type: String},
        createdeAt: {type: String, default: new Date().toLocaleString()}
    });

userSchema.statics.createUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, null, function (err, hash) {
            if (err) return console.log('Hash generation error');
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};

userSchema.statics.comparePass = function (enteredPassword, dbPassword, callback) {
    bcrypt.compare(enteredPassword, dbPassword, function (err, isMatch) {
        if (err) throw err;
        callback(null, isMatch);
    })
};

userSchema.statics.isLoggedIn = async function(req, res){
    if (!req.user) {
        res.redirect('/users/login');
    }
};

userSchema.statics.validateRegisterForm = async function (req) {
    req.checkBody('username', 'Name is required').notEmpty();

    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Please enter a valid email').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('retype_password', 'Passwords do not match').equals(req.body.password);

    let errors = await req.getValidationResult();
    return errors.throw();
};


let Model = mongoose.model('User', userSchema);
module.exports = Model;