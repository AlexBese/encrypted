let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
	
let economiiSchema = new Schema({
    suma: { type: String, required: true},
    target: { type: String, required: true},
    date: { type: String, default: new Date()},
    categoria: {type: String, required: true},
    user: { type: mongoose.Schema.Types.ObjectId, ref:'User' }
});

economiiSchema.statics.validateForm = async function (req) {
    req.checkBody('suma', 'Trebuie să introduceți suma').notEmpty();
    req.checkBody('suma', 'Suma trebuie sa fie un numar').isNumeric();
    req.checkBody('target', 'Trebuie să introduceți target').notEmpty();
    req.checkBody('target', 'Targetul trebuie sa fie un numar').isNumeric();
    req.checkBody('date', 'Trebuie să introduceți data').notEmpty();
    req.checkBody('categoria', 'Trebuie să introduceți o categorie').notEmpty();

    let errors = await req.getValidationResult();
    return errors.throw();
};

let Model = mongoose.model('Economii', economiiSchema);
module.exports = Model;