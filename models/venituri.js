let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
	
let venituriSchema = new Schema({
    suma: { type: Number, required: true},
    date: { type: String, default: new Date()},
    cont: { type: String },
    moneda: {type: String, required: true},
    user: { type: mongoose.Schema.Types.ObjectId, ref:'User' }
});

venituriSchema.statics.validateForm = async function (req) {
    let monede = {"RON":"RON", "EUR":"EUR"}
    req.checkBody('moneda', 'Moneda este obligatorie').notEmpty();
    req.checkBody('moneda', 'Moneda pe care ai ales-o nu face parte din cele date').dropdownValidator(monede);
    req.checkBody('suma', 'Suma este obligatorie').notEmpty();
    req.checkBody('suma', 'Suma trebuie să fie un număr').isNumeric(monede);
    req.checkBody('cont', 'Contul este obligatoriu').notEmpty(monede);

    let errors = await req.getValidationResult();
    return errors.throw();
};

let Model = mongoose.model('Venituri', venituriSchema);
module.exports = Model;