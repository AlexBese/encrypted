'use strict';

let LocalStrategy = require('passport-local').Strategy,
    userModel = require('../../models/utilizatori');

module.exports = function (passport) {
    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallBack: true
    }, function (email, password, done) {
        userModel.findOne(
            {'email': email},
            function (err, user) {
                if (err) console.log(err);
                if (user) {
                    userModel.comparePass(password, user.password, function (err, res) {
                        if (res === true) {
                            done(null, user);
                        } else {
                            done(null, false, {message : "Email or password incorrect"});
                        }
                    })
                } else {
                    done(null, false, {message : "Email or password incorrect"});
                }
            }
        );
    }));
};
