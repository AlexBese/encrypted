'use strict';

const userModel = require('../models/utilizatori');

let userController = function () {
    let getLogin = function (req, res, next) {
        res.render('login', {title: 'Express'});
    };

    let getRegister = async function (req, res, next) {
        res.render('users/register');
    };

    let postRegisterOrUpdate = async function (req, res, next) {
        try {
            await userModel.validateRegisterForm(req);
            let user = await userModel.findOne({email: req.body.email});

            if (user) {
                res.render('users/register', {
                    errors: { email: { param: 'email', msg: 'Email already taken', value: '' }},
                    errorBody: req.body
                });
            } else {
                let newUser = new userModel(req.body);

                userModel.createUser(newUser, function (err, user) {
                    if (err) throw err;
                });
                res.redirect('/login');
            }
        } catch (errors) {
            res.render('users/register', {
                errors: errors.mapped(),
                errorBody: req.body,
            });
        }
    };

    let getProfile = function (req, res, next) {
        res.json(req.user);
    };

    let isLoggedIn = function (req, res, next) {
        if (!req.user) {
            console.log("not logged in!");
            res.redirect('/login');
        }
        next();
    };

    let getLogOut = function (req, res) {
        req.logout();
        //
        res.redirect('/');
    };

    return {
        getLogin: getLogin,
        getRegister: getRegister,
        postRegisterOrUpdate: postRegisterOrUpdate,
        getProfile: getProfile,
        isLoggedIn: isLoggedIn,
        getLogOut: getLogOut
    };
};

module.exports = userController;
