var express = require('express');
var router = express.Router();
var cheltuieliModel= require('../models/cheltuieli');
var conturiModel = require('../models/conturi');

async function checkAuth(req, res){
    if (!req.user) {
        res.redirect('/login');
    }
}

/* GET home page. */
router.get('/', async function(req, res, next) {
  await checkAuth(req, res);
  let cheltuieli = await cheltuieliModel.find({ user: req.user._id });
  let conturi = await conturiModel.find({ user: req.user._id });

  res.render('cheltuieli',
  {
    conturi: conturi,
    cheltuieli: cheltuieli
  });
});

router.post('/', async function(req, res, next) {
  await checkAuth(req, res);
  try{
    await cheltuieliModel.validateForm(req);

    var cheltuialaMea = new cheltuieliModel({
      suma: req.body.suma,
      cont: req.body.cont,
      moneda: req.body.moneda,
      categoria: req.body.categoria,
      date: req.body.date,
      user: req.user._id
    });
    cheltuialaMea.save();
  
    res.redirect("/cheltuieli");
  } catch (errors){
     let cheltuieli = await cheltuieliModel.find({ user: req.user._id });
     let conturi = await conturiModel.find({ user: req.user._id });
      res.render('cheltuieli',
      { 
        errors: errors.mapped(),
        conturi: conturi,
        cheltuieli: cheltuieli
      });
    
  }
  
});

router.post('/ajax', async function(req, res, next) {
    await checkAuth(req, res);
    try{
        await cheltuieliModel.validateForm(req);
        var cheltuialaMea = new cheltuieliModel({
            suma: req.body.suma,
            cont: req.body.cont,
            moneda: req.body.moneda,
            categoria: req.body.categoria,
            date: req.body.date,
            user: req.user._id
        });
        cheltuialaMea.save();
        res.status(200).json({ "Success" : cheltuialaMea });
    } catch (errors){
        res.status(500).json({ "err" : errors.mapped() });
    }
});

router.get('/edit/:id', async function(req, res, next){
  await checkAuth(req, res);
  let cheltuieli = await cheltuieliModel.findById(req.params.id);
  let conturi = await conturiModel.find({ user: req.user._id });

  res.render('cheltuieliEdit',
  {
    conturi: conturi,
    cheltuieli : cheltuieli
  });
});

router.post('/edit/:id', async function(req,res,next){
    await checkAuth(req, res);
    let id = req.params.id;
    try{
      await cheltuieliModel.validateForm(req);
      await cheltuieliModel.findByIdAndUpdate(id, {$set: req.body});
    } catch(errors){
      let cheltuieli = await cheltuieliModel.findById(req.params.id);
      let conturi = await conturiModel.find({ user: req.user._id });
       res.render('cheltuieliEdit',
       { 
         errors: errors.mapped(),
         cheltuieli: cheltuieli,
         conturi: conturi
       });
    }

    res.redirect("/cheltuieli");
});

router.get('/delete/:id', async function(req, res, next) {
  await checkAuth(req, res);
  await cheltuieliModel.findByIdAndRemove(req.params.id);
  res.redirect("/cheltuieli");
});


module.exports = router;
