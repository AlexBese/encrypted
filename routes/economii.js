var express = require('express');
var router = express.Router();
var economiiModel = require('../models/economii');

async function checkAuth(req, res){
    if (!req.user) {
        res.redirect('/login');
    }
}
/* GET home page. */
router.get('/', async function(req, res, next) {
    await checkAuth(req, res);
    let economii = await economiiModel.find({ user: req.user._id });
    res.render('economii',
    {
        economii: economii
    });
});

router.post('/', async function(req, res, next) {
  await checkAuth(req, res);
  try{
    await economiiModel.validateForm(req);
    let economiaMea = new economiiModel({
      suma: req.body.suma,
      target: req.body.target,
      date: req.body.date,
      categoria: req.body.categoria,
      user: req.user._id
    });

    economiaMea.save();
    res.redirect("/economii");

  } catch (errors){
      let economii = await economiiModel.find();
      res.render('economii',
      { 
        errors: errors.mapped(),
        economii: economii
      });
  }
});

router.get('/edit/:id', async function(req, res, next){
  await checkAuth(req, res);
  let economie = await economiiModel.findById(req.params.id);
  res.render('economiiEdit',
  {
      economie : economie
  });
});

router.post('/edit/:id', async function(req,res,next){
    await checkAuth(req, res);
    let id = req.params.id;
    try{
      await economiiModel.validateForm(req);
      await economiiModel.findByIdAndUpdate(id, {$set: req.body});
    } 
    catch (errors){
       let economie = await economiiModel.findById(req.params.id);
       res.render('economiiEdit',
       { 
         errors: errors.mapped(),
           economie: economie
       });
   }

    res.redirect("/economii");
});

router.get('/delete/:id', async function(req, res, next) {
  await checkAuth(req, res);
  await economiiModel.findByIdAndRemove(req.params.id);
  res.redirect("/economii");
});

module.exports = router;
