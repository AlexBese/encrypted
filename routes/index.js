var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var venituriModel = require('../models/venituri');
var cheltuieliModel= require('../models/cheltuieli');
var economiiModel = require('../models/economii');
var transporter = nodemailer.createTransport( {
  "service": "Gmail",
  "auth": {
  "user": "encryptedteam06@gmail.com",
  "pass": "encrypTed06"
  }
  });

async function checkAuth(req, res){
    if (!req.user) {
        res.redirect('/login');
    }
}

/* GET home page. */
router.get('/', function(req, res, next) {  
  res.render('index', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/dashboard', function(req, res, next) {  
  res.render('dashboard', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/ecommerce', function(req, res, next) {  
  res.render('ecommerce', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/analytics', function(req, res, next) {  
  res.render('analytics', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});


router.get('/login', function(req, res, next) {  
  res.render('login', 
  { 
    title: 'login',
    age: 0,
    dashboardText: 'MyDash'
    
  }
);
});

router.get('/register', function(req, res, next) {  
  res.render('register', 
  { 
    title: 'register',
    age: 0,
    dashboardText: 'MyDash'
    
  }
);
});
router.get('/servicii', function(req, res, next) {  
  res.render('servicii', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/articole', function(req, res, next) {  
  res.render('articole', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/contact', function(req, res, next) {  
  res.render('contact', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});

router.get('/articoledash', function(req, res, next) {  
  res.render('articoledash', 
  { 
    title: 'PiggyBank',
    age: 0,
    dashboardText: 'MyDash'
  }
);
});


router.post('/contact', async function(req, res, next) {  

    try {
        let options = {
                from: req.body.email, // sender address
                to: 'encryptedteam06@gmail.com', // list of receivers //TODO: Add Email parameter
                subject: 'message', // Subject line
                html: req.body.message
            }
             await transporter.sendMail(options);

        //console.log('Message %s sent: %s', info.messageId, info.response);
        console.log('Message not sent - temporary disabled removed pass from config for email security');
    } catch (err) {
        console.log('Error occured in sending mail reason: ' + err);
   }  
}

);
router.get('/suport', async function(req, res, next) {
    if (!req.user) {
        res.redirect('/login');
    } else {
        res.render('suport',
            {
                title: 'PiggyBank',
                age: 0,
                dashboardText: 'MyDash'
            });
    }
});

router.get('/grafice', async function(req, res, next) {
    await checkAuth(req, res);
    let venituri = await venituriModel.find({ user: req.user._id });
    let cheltuieli = await cheltuieliModel.find({ user: req.user._id });
    let economii = await economiiModel.find({ user: req.user._id });

    res.render('grafice',
        {
            venituri : venituri,
            cheltuieli : cheltuieli,
            economii : economii
        }
    );
});

module.exports = router;
