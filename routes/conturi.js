var express = require('express');
var router = express.Router();
var conturiModel = require('../models/conturi');

async function checkAuth(req, res){
    if (!req.user) {
        res.redirect('/login');
    }
}
/* GET home page. */
router.get('/', async function(req, res, next) {
    await checkAuth(req, res);
    let conturi = await conturiModel.find({ user: req.user._id });
    res.render('conturi',
    {
        conturi: conturi
    });
});

router.post('/', async function(req, res, next) {
  await checkAuth(req, res);
  try{
    await conturiModel.validateForm(req);
    let contulMeu = new conturiModel({
      numar: req.body.numar,
      user: req.user._id
    });

    contulMeu.save();
    res.redirect("/conturi");

  } catch (errors){
      let conturi = await conturiModel.find();
      res.render('conturi',
      { 
        errors: errors.mapped(),
        conturi: conturi
      });
  }
});

router.get('/edit/:id', async function(req, res, next){
  await checkAuth(req, res);
  let cont = await conturiModel.findById(req.params.id);
  res.render('conturiEdit',
  {
    cont : cont
  });
});

router.post('/edit/:id', async function(req,res,next){
    await checkAuth(req, res);
    let id = req.params.id;
    try{
      await conturiModel.validateForm(req);
      await conturiModel.findByIdAndUpdate(id, {$set: req.body});
    } 
    catch (errors){
      let cont = await conturiModel.findById(req.params.id);
       res.render('conturiEdit',
       { 
         errors: errors.mapped(),
         cont: cont
       });
   }

    res.redirect("/conturi");
});

router.get('/delete/:id', async function(req, res, next) {
  await checkAuth(req, res);
  await conturiModel.findByIdAndRemove(req.params.id);
  res.redirect("/conturi");
});

module.exports = router;
