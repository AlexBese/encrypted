var express = require('express');
var router = express.Router();
var venituriModel = require('../models/venituri');
var conturiModel = require('../models/conturi');

async function checkAuth(req, res){
    if (!req.user) {
        res.redirect('/login');
    }
}
/* GET home page. */
router.get('/', async function(req, res, next) {
    await checkAuth(req, res);
    let venituri = await venituriModel.find({ user: req.user._id });
    let conturi = await conturiModel.find({ user: req.user._id });

    res.render('venituri',
    {
      conturi: conturi,
      venituri: venituri
    });
});

router.post('/', async function(req, res, next) {
  await checkAuth(req, res);
  try{
    await venituriModel.validateForm(req);
    var venitulMeu = new venituriModel({
      suma: req.body.suma,
      cont: req.body.cont,
      moneda: req.body.moneda,
      date: req.body.date,
      user: req.user._id
    });
    venitulMeu.save();
  
    res.redirect("/venituri");
  } catch (errors){
     let conturi = await conturiModel.find();
     let venituri = await venituriModel.find();
      res.render('venituri',
      { 
        errors: errors.mapped(),
        venituri: venituri,
        conturi: conturi
      });
    
  }
});

router.post('/ajax', async function(req, res, next) {
  await checkAuth(req, res);
  try{
    await venituriModel.validateForm(req);
    var venitulMeu = new venituriModel({
      suma: req.body.suma,
      cont: req.body.cont,
      moneda: req.body.moneda,
      date: req.body.date,
      user: req.user._id
    });
    venitulMeu.save();
    res.status(200).json({ "Success" : venitulMeu });
  } catch (errors){
    res.status(500).json({ "err" : errors.mapped() });
  }
});

router.get('/edit/:id', async function(req, res, next){
  await checkAuth(req, res);
  let venit = await venituriModel.findById(req.params.id);
  let conturi = await conturiModel.find({ user: req.user._id });

  res.render('venituriEdit',
  {
    conturi: conturi,
    venit : venit
  });
});

router.post('/edit/:id', async function(req,res,next){
    await checkAuth(req, res);
    let id = req.params.id;
    try{
      await venituriModel.validateForm(req);
      await venituriModel.findByIdAndUpdate(id, {$set: req.body});
    } 
    catch (errors){
      let venit = await venituriModel.findById(req.params.id);
      let conturi = await conturiModel.find({ user: req.user._id });
       res.render('venituriEdit',
       { 
         errors: errors.mapped(),
         conturi: conturi,
         venit: venit
       });
   }

    res.redirect("/venituri");
});

router.get('/delete/:id', async function(req, res, next) {
  await checkAuth(req, res);
  await venituriModel.findByIdAndRemove(req.params.id);
  res.redirect("/venituri");
});

module.exports = router;
