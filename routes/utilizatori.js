'use strict';

let express = require('express'),
    authRouter = express.Router(),
    userController = require('../controllers/utilizatoriController')();

let router = function (passport) {
    //logic for an entire module to need to login
    // authRouter.use(function(req, res, next){
    //     if(!req.user){
    //         res.redirect('login');
    //     }
    //     next();
    // });
    // or use controller function : authRouter.use(userController.isLoggedIn)

    authRouter.route('/login')
        .post(passport.authenticate('local', {
            failureRedirect: '/login',
            failureFlash: true
        }), function (req, res) {
            //
            res.redirect('/grafice');
        });
    authRouter.route('/register')
        .post(userController.postRegisterOrUpdate);

    authRouter.route('/profile')
        .all(userController.isLoggedIn)
        .get(userController.getProfile);

    authRouter.route('/logout')
        .all(userController.isLoggedIn)
        .get(userController.getLogOut);

    return authRouter;
};

module.exports = router;
